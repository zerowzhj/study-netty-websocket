package study.netty.websocket.support;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "study.netty.websocket")
public class SpringBootCfg {
}
