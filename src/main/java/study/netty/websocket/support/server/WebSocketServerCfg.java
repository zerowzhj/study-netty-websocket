package study.netty.websocket.support.server;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class WebSocketServerCfg {

    @Bean
    @ConfigurationProperties(prefix = "socket")
    public ServerProperties serverProperties() {
        return new ServerProperties();
    }

    @Bean(initMethod = "start", destroyMethod = "shutdown")
    public WebSocketServer webSocketServer(ServerProperties prop) {
        WebSocketServer server = new WebSocketServer();
        return server;
    }
}
